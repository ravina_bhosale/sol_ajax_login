﻿using Sol_AJAX_Login.DAL.ORD;
using Sol_AJAX_Login.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_AJAX_Login.DAL
{
    public class UserDal
    {
        #region Declaration
        private UserDcDataContext dc = null;
        #endregion

        #region Constructor
        public UserDal()
        {
            dc = new UserDcDataContext();
        }
        #endregion

        #region Property
        private Func<UserResultSet,UserEntity> SelectUserData
        {
            get
            {
                return
                       (leUserResultSet) => new UserEntity()
                       {
                           UserId = leUserResultSet.UserID,
                           FirstName = leUserResultSet.FirstName,
                           LastName = leUserResultSet.LastName
                       };
            }
        }
        #endregion 

        public async Task<dynamic> CheckLoginAsync(UserEntity userEntityObj)
        {
            string message = null;
            try
            {
                return await Task.Run(() =>
                {

                    var getQuery =
                        dc
                       ?.uspLogin(
                            userEntityObj?.UserLogin?.UserName,
                            userEntityObj?.UserLogin?.Password,
                            ref message
                            )
                         ?.AsEnumerable()
                         ?.Select(this.SelectUserData)
                         ?.FirstOrDefault();

                    var result = (message.Contains("does not match")) 
                                            ? (dynamic) message 
                                            : getQuery;

                    return result;
                });
            }
            catch(Exception)
            {
                throw;
            }
        }
    }
}