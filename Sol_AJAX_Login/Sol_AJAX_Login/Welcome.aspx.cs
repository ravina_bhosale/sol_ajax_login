﻿using Newtonsoft.Json;
using Sol_AJAX_Login.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Sol_AJAX_Login
{
    public partial class Welcome : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack == false)
                {
                    await this.BindUserData();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Private Method
        private async Task<Boolean> IsValidSession()
        {
            try
            {
                return await Task.Run(() =>
                {

                    bool flag = (Session["UserInfo"] != null) ? true : false;

                    return flag;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task BindUserData()
        {
            try
            {
                await Task.Run(async () =>
                {

                    if (await IsValidSession())
                    {

                        // get Session Value
                        string userJsonData = Session["UserInfo"].ToString();

                        // Deserialize Json String into User Entity Object
                        UserEntity userEntityObj = JsonConvert.DeserializeObject<UserEntity>(userJsonData);

                        // Bind Full Name into Label Control

                        lblFullName.Text = new StringBuilder()
                                                    .Append("Welcome ")
                                                    .Append(userEntityObj.FirstName)
                                                    .Append(" ")
                                                    .Append(userEntityObj.LastName)
                                                    .ToString();

                    }
                    else
                    {
                        Response.Redirect("~/Default.aspx", false);
                    }
                });


            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

    protected async void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                await Task.Run(() => { 
                    
                    Session.Remove("UserInfo");
                    Session.Abandon();

                    Response.Redirect("~/Default.aspx", false);

                });

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
