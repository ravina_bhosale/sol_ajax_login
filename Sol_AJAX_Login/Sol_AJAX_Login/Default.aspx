﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sol_AJAX_Login.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>Login</title>

</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

          <asp:UpdatePanel ID="UpdatePanel1" runat="server">

              <ContentTemplate>

                    <table>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" placeHolder="User Name"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" placeHolder="Password" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                            <asp:Button ID="btnSubmit" runat="server" Text="Log in" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>

              </ContentTemplate>

        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
