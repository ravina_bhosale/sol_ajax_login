﻿using Newtonsoft.Json;
using Sol_AJAX_Login.DAL;
using Sol_AJAX_Login.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_AJAX_Login
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                // Mapping
                var userEntityObj = await this.MapUserData();

                // Check Valid Login 
                var result = await new UserDal().CheckLoginAsync(userEntityObj);

                // Bind and Page Redirection
                await this.BindandRedirection(result);
            }
            catch (Exception)
            {
                throw;
            }

        }

        #region Private Method
        private async Task<UserEntity> MapUserData()
        {
            try
            {
                return await Task.Run(() =>
                {

                    // Create an instance of Uuser Entity and Map control data into Poco
                    UserEntity userEntityObj = new UserEntity()
                    {
                        UserLogin = new UserLoginEntity()
                        {
                            UserName = txtUserName.Text,
                            Password = txtPassword.Text
                        }
                    };

                    return userEntityObj;
                });


            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task BindandRedirection(dynamic result)
        {
            try
            {
                await Task.Run(async () =>
                {

                    // true part..login Success
                    if (result is UserEntity)
                    {
                        // Create a Session
                        await this.CreateUserSession(result as UserEntity);

                        // Redirect to Welcome page
                        Response.Redirect("~/Welcome.aspx", false);
                    }
                    else // false Part .. Login Failed
                    {
                        // show Error Message
                        lblErrorMessage.Text = result;
                    }

                });


            }
            catch (Exception)
            {
                throw;
            }
        }

        private async Task CreateUserSession(UserEntity userEntity)
        {
            try
            {
                await Task.Run(() =>
                {

                    // Serialize User Entity Object into JSON
                    string userJsonData = JsonConvert.SerializeObject(userEntity);

                    // Create a Session Value
                    Session["UserInfo"] = userJsonData;
                });



            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
