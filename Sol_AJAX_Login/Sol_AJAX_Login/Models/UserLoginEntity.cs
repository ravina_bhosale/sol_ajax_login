﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_AJAX_Login.Models
{
    public class UserLoginEntity
    {
        public decimal? UserId { get; set; }

        public String UserName { get; set; }

        public String Password { get; set; }
    }
}