﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_AJAX_Login.Models
{
    public class UserEntity
    {
        public decimal? UserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public UserLoginEntity UserLogin { get; set; }
    }
}