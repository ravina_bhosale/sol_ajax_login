﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Sol_AJAX_Login.Models;
using Sol_AJAX_Login.DAL;

namespace UTP
{
    [TestClass]
    public class UserUnitTest
    {
        [TestMethod]
        public void CheckLoginTestMethod()
        {
            Task.Run(async () =>
            {

                var userEntityObj = new UserEntity()
                {
                    UserLogin = new UserLoginEntity()
                    {
                        UserName = "ravina",
                        Password = "1234"
                    }
                };

                var result =  await new UserDal().CheckLoginAsync(userEntityObj);

                Assert.IsNotNull(result);

            }).Wait();
        }
    }
}
